<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

  
<!DOCTYPE html>
<c:set var="view" value="/kategorija" scope="session" />
<html>
        <body>
    
<div id="categoryLeftColumn">
    
    <c:forEach var="objekatKat" items="${objekatKategorije}">

        <c:choose>
            <c:when test="${objekatKat.naziv_Kategorije == selektovanaKategorija.naziv_Kategorije}">
                <div class="categoryButton" id="selectedCategory">
                    <span class="categoryText">
                        ${objekatKat.naziv_Kategorije}
                    </span>
                </div>
            </c:when>
            <c:otherwise>
                <a href="kategorija?${objekatKat.id_Kategorija}" class="categoryButton">
                    <div class="categoryText">
                        ${objekatKat.naziv_Kategorije}
                    </div>
                </a>
            </c:otherwise>
        </c:choose>

    </c:forEach>

</div>
            <div id="categoryRightColumn">
                <p id="categoryTitle">${selektovanaKategorija.naziv_Kategorije}</p>

                <table id="productTable">
                   <c:forEach var="proizvod" items="${kategorijaProizvoda}" varStatus="iter">

        <tr class="${((iter.index % 2) == 0) ? 'lightBlue' : 'white'}">
            <td>
                ${proizvod.ime_Proizvoda}
                <br>
                <span class="smallText">${proizvod.opis_Sastojaka}</span>
            </td>
            <td>
                RSD ${proizvod.cena} / 1
            </td>
            <td>
                <form action="<c:url value='ubaciUKorpu'/>" method="post">
                    <input type="hidden"
                           name="prozivodId"
                           value="${proizvod.id_Proizvod}">
                    <input type="submit"
                           value="Dodaj u korpu">
                </form>
            </td>
        </tr>

    </c:forEach>
                </table>
            </div>

            
     
    </body>
</html>
