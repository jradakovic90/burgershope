<!DOCTYPE html>

<c:set var="view" value="/korpa" scope="session"/>
<html>
   <body>

            <div id="centerColumn">

                <p>Vasa korpa sadrzi ${korpa.numberOfItems} proizvoda.</p>
                <div id="actionBar">
        <%-- clear cart widget --%>
        <c:if test="${!empty korpa && korpa.numberOfItems != 0}">

            <c:url var="url" value="pogledajKorpu">
                <c:param name="obrisi" value="true"/>
            </c:url>

            <a href="${url}" class="bubble hMargin">Obrisi korpu</a>
        </c:if>

        <%-- continue shopping widget --%>
        <c:set var="value">
            <c:choose>
                <%-- if 'selectedCategory' session object exists, send user to previously viewed category --%>
                <c:when test="${!empty selektovanaKategorija}">
                    kategorija
                </c:when>
                <%-- otherwise send user to welcome page --%>
                <c:otherwise>
                    index.jsp
                </c:otherwise>
            </c:choose>
        </c:set>

        <c:url var="url" value="${value}"/>
        <a href="${url}" class="bubble hMargin">Nastavi kupovinu</a>

        <%-- checkout widget --%>
        <c:if test="${!empty korpa && korpa.numberOfItems != 0}">
            <a href="<c:url value='racun'/>" class="bubble hMargin">Uzmite racun</a>
        </c:if>
    </div>
            <c:if test="${!empty korpa && korpa.numberOfItems != 0}">

      <h4 id="subtotal">Total:
          ${korpa.subtotal}
      </h4>

      <table id="cartTable">

        <tr class="header">
            <th>Ime proizvoda</th>
            <th>Cena po komadu</th>
            <th>Kolicina</th>
            
        </tr>

        <c:forEach var="cartItem" items="${korpa.items}" varStatus="iter">

          <c:set var="product" value="${cartItem.product}"/>

          <tr class="${((iter.index % 2) == 0) ? 'lightBlue' : 'white'}">
            <td>${product.ime_Proizvoda}</td>

            <td>
                <span class="smallText">
                    ${product.cena}
                    </span>
               <br>
                 
            </td>

            <td>
                <form action="<c:url value='updateKorpu'/>" method="post">
                    <input type="hidden"
                           name="id_Proizvod"
                           value="${product.id_Proizvod}">
                    <input type="text"
                           maxlength="2"
                           size="2"
                           value="${cartItem.kolicina}"
                           name="kolicina"
                           style="margin:5px">
                    <input type="submit"
                           name="submit"
                           value="Osvezi korpu">
                </form>
            </td>
          </tr>

        </c:forEach>

      </table>

    </c:if>
             </div>

           
   
    </body>
</html>
