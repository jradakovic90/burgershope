<!DOCTYPE html>
<html>
   <body>

           <div id="singleColumn">

    <p id="confirmationText">
        <strong>Uspesna kupovina</strong>
    </p>

    <div class="summaryColumn" >

        <table id="orderSummaryTable" class="detailsTable">
            <tr class="header">
                <th colspan="3">Ovo ste narucili</th>
            </tr>

            <tr class="tableHeading">
                <td>Proizvod</td>
                <td>Kolicina</td>
                <td>Cena</td>
            </tr>

            <c:forEach var="orderedProduct" items="${naruceniProizvodi}" varStatus="iter">

                <tr class="${((iter.index % 2) != 0) ? 'lightBlue' : 'white'}">
                    <td>
                        ${proizvodi[iter.index].ime_Proizvoda}
                    </td>
                    <td class="quantityColumn">
                        ${orderedProduct.kolicina}
                    </td>
                    <td class="confirmationPriceColumn">
                       ${proizvodi[iter.index].cena * orderedProduct.kolicina}
                    </td>
                </tr>

            </c:forEach>

            <tr class="lightBlue"><td colspan="3" style="padding: 0 20px"><hr></td></tr>

            <tr class="lightBlue">
                <td colspan="2" id="totalCellLeft"><strong>Total:</strong></td>
                <td id="totalCellRight">
                    ${porudzbina.racunPorudzbine}</td>
            </tr>

            <tr class="lightBlue"><td colspan="3" style="padding: 0 20px"><hr></td></tr>

            <tr class="lightBlue">
                <td colspan="3" id="dateProcessedRow"><strong>Datuma:</strong>
                    ${porudzbina.vremekreiranjaPorudzbine}"
                                   </td>
            </tr>
        </table>

    </div>

    <div class="summaryColumn" >

        <table id="deliveryAddressTable" class="detailsTable">
            <tr class="header">
                <th colspan="3">Adresa:</th>
            </tr>

            <tr>
                <td colspan="3" class="lightBlue">
                    <strong>Adresa:</strong> ${kupac.adresa_Kupca}
                    <br>
                    <strong>Telefon:</strong> ${kupac.broj_Telefona}
                </td>
            </tr>
        </table>
    </div>
</div>

            
      </body>
</html>
