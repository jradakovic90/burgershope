<!DOCTYPE html>
<c:set var="view" value="/racun" scope="session"/>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<html>
   <body>

       <script type="text/javascript">

    $(document).ready(function(){
        $("#checkoutForm").validate({
            rules: {
                
                phone: {
                    required: true,
                    number: true,
                    minlength: 9
                },
                address: {
                    required: true
                }
            }
        });
    });
</script>


<%-- HTML markup starts below --%>

<div id="singleColumn">

    <h2>Racun</h2>

    

    <c:if test="${!empty validationErrorFlag}">
        <p class="error"><font color="red">Greska u racunu</font></p>
    </c:if>

    <form id="checkoutForm" action="<c:url value='kupovina'/>" method="post">
        <table id="checkoutTable">
          <c:if test="${!empty validationErrorFlag}">
            <tr>
                <td colspan="2" style="text-align:left">
                    <span class="error smallText"><font color="red">Molimo vas popravite unose</font>
                      <c:if test="${!empty phoneError}">
                          <br><span class="indent"><font color="red">Telefon pogresno unet</font></span>
                      </c:if>
                      <c:if test="${!empty addressError}">
                      <br><span class="indent"><font color="red">Adresa pogresno uneta</font></span>
                      </c:if>
                    </span>
                </td>
            </tr>
          </c:if>
            <tr>
                <td><label for="phone">Telefon:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="16"
                           id="phone"
                           name="phone"
                           value="${param.phone}">
                </td>
            </tr>
            <tr>
                <td><label for="address">Adresa:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="45"
                           id="address"
                           name="address"
                           value="${param.address}">

                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Potvrdi">
                </td>
            </tr>
        </table>
    </form>

    <div id="infoBox">

        

        <table id="priceBox">
            <tr>
                <td class="total">Ukupno:</td>
                <td class="total checkoutPriceColumn">
                ${korpa.total}</td>
            </tr>
        </table>
    </div>
</div>
      </body>
</html>
