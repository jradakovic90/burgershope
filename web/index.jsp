<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/JelenasBurgers.css">
    <title>Jelenas burgers</title>
    </head>
    
    <body>
    <div id="main">
        <div id="header">
            <div id="widgetBar">
  </div>
 
    <a href="http://localhost:8080/JelenasBurgers/">
    
       <img src="${initParam.Image}logo jelenas.jpg" id="logo"
                         alt="JB logo" style="width:100px;height:100px;">
    </a>
           
              <div id="nazivFirme"><h1><br></br>Jelenas burgers </h1>
   </div>
        </div>
        <div id="indexLeftColumn">
         <div id="welcomeText">
        <p>Prodavnica burgera Jelenas Burgers Vam nudi veliku ponudu svetski poznatih burgera. </br>
Burgerdzinica je poznata po dobijanju prve nagrade za spremanje najukusnijeg burgera 2016. godine, na takmicenju u pravljenju burgera u Njujorku. </br>
 Uz vrhunske majstore pravljenja burgera i uz ljubazno osoblje dodjite da uronite u potpuno novi svet magicnih burgera.</br>
</p>
       
       
    </div>
        </div>
         <div id="indexRightColumn">
           
     <c:forEach var="objekatKat"  items="${objekatKategorije}">
         <div class="categoryBox">
                        <a href="kategorija?${objekatKat.id_Kategorija}">

                            <span class="categoryLabelText">${objekatKat.naziv_Kategorije}</span>
                            <p></p>
        <img src="${initParam.Image}${objekatKat.naziv_Kategorije}.jpg"
                                 alt="${objekatKat.naziv_Kategorije}" style="width:150px;height:120px;">
                        </a>
                         </div>
                        </c:forEach>   
   
        </div>

        <div id="footer">
         <hr>
    <p id="footerText">Jelenas Burgers 2016 &trade;</p>
        </div>
    </div>
</body>
</html>
