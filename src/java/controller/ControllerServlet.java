/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import cart.ShoppingCart;
import entity.Kategorija;
import entity.Porudzbina;
import entity.Proizvod;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import session.KategorijaFacade;
import session.PorudzbinaManager;
import session.ProizvodFacade;
import validate.Validator;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "ControllerServlet",
        loadOnStartup = 1,
        urlPatterns = {"/kategorija",
            "/ubaciUKorpu",
            "/pogledajKorpu",
            "/updateKorpu",
            "/racun",
            "/kupovina"})

public class ControllerServlet extends HttpServlet {

    @EJB
    private KategorijaFacade kategorijaFacade;

    @EJB
    private ProizvodFacade proizvodFacade;

    @EJB
    private PorudzbinaManager porudzbinaManager;

    @Override
    public void init() throws ServletException {

        // store category list in servlet context
        getServletContext().setAttribute("objekatKategorije", kategorijaFacade.findAll());
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Kategorija selektovanaKategorija;
        Collection<Proizvod> proizvodCollection;
        // if category page is requested
        if (userPath.equals("/kategorija")) {
            String kategorijaId = request.getQueryString();
            if (kategorijaId != null) {
                // get selected category
                selektovanaKategorija = kategorijaFacade.find(Integer.parseInt(kategorijaId));
                // place selected category in request scope
                session.setAttribute("selektovanaKategorija", selektovanaKategorija);
                // get all products for selected category
                proizvodCollection = selektovanaKategorija.getProizvodCollection();
                // place category products in request scope
                session.setAttribute("kategorijaProizvoda", proizvodCollection);
            }
            // if cart page is requested
        } else if (userPath.equals("/pogledajKorpu")) {
            String obrisi = request.getParameter("obrisi");

            if ((obrisi != null) && obrisi.equals("true")) {

                ShoppingCart cart = (ShoppingCart) session.getAttribute("korpa");
                cart.clear();
            }

            userPath = "/korpa";

            // if checkout page is requested
        } else if (userPath.equals("/racun")) {
            ShoppingCart cart = (ShoppingCart) session.getAttribute("korpa");
            // calculate total
            cart.calculateTotal();
            // forward to checkout page and switch to a secure channel
            userPath = "/racun";
        }

        // use RequestDispatcher to forward request internally
        String url = "/WEB-INF/view" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        ShoppingCart korpa = (ShoppingCart) session.getAttribute("korpa");
        Validator validator = new Validator();

        // if addToCart action is called
        if (userPath.equals("/ubaciUKorpu")) {
            // TODO: Implement add proizvod to korpa action
            if (korpa == null) {
                korpa = new ShoppingCart();
                session.setAttribute("korpa", korpa);
            }
            String idProizvod = request.getParameter("prozivodId");

            if (idProizvod != null && !idProizvod.isEmpty()) {

                Proizvod proizvod = proizvodFacade.find(Integer.parseInt(idProizvod));
                korpa.addItem(proizvod);
            }

            userPath = "/kategorija";

            // if updateCart action is called
        } else if (userPath.equals("/updateKorpu")) {
            // get input from request
            String idProizvod = request.getParameter("id_Proizvod");
            String kolicina = request.getParameter("kolicina");

            boolean invalidEntry = validator.validateQuantity(idProizvod, kolicina);

            if (!invalidEntry) {

                Proizvod proizvod = proizvodFacade.find(Integer.parseInt(idProizvod));
                korpa.update(proizvod, kolicina);
            }

            userPath = "/korpa";

            // if purchase action is called
        } else if (userPath.equals("/kupovina")) {
            // TODO: Implement kupovina action
            if (korpa != null) {

                String phone = request.getParameter("phone");
                String address = request.getParameter("address");

                // validate user data
                boolean validationErrorFlag = false;
                validationErrorFlag = validator.validateForm(phone, address, request);

                // if validation error found, return user to checkout
                if (validationErrorFlag == true) {
                    request.setAttribute("validationErrorFlag", validationErrorFlag);
                    userPath = "/racun";

                    // otherwise, save order to database
                } else {

                    Porudzbina porudzbina = porudzbinaManager.postaviPorudzbinu(phone, address, korpa);
                    // if order processed successfully send user to confirmation page
                    if (porudzbina != null) {

                        // dissociate shopping cart from session
                        korpa = null;

                        // end session
                        session.invalidate();

                        // get order details
                        Map orderMap = porudzbinaManager.getDetaljiPorudzbine(porudzbina);

                        // place order details in request scope
                        request.setAttribute("kupac", orderMap.get("kupac"));
                        request.setAttribute("porudzbina", orderMap.get("porudzbina"));
                        request.setAttribute("proizvodi", orderMap.get("proizvodi"));
                        request.setAttribute("naruceniProizvodi", orderMap.get("naruceniProizvodi"));

                        userPath = "/potvrda";

                    } else {
                        userPath = "/racun";
                        request.setAttribute("orderFailureFlag", true);
                    }
                }
            }
        }
        // use RequestDispatcher to forward request internally
        String url = "/WEB-INF/view" + userPath + ".jsp";

        try {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
            requestDispatcher.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
