/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "kupac")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kupac.findAll", query = "SELECT k FROM Kupac k"),
    @NamedQuery(name = "Kupac.findById_Kupac", query = "SELECT k FROM Kupac k WHERE k.id_Kupac = :id_Kupac"),
    @NamedQuery(name = "Kupac.findByAdresa_Kupca", query = "SELECT k FROM Kupac k WHERE k.adresa_Kupca = :adresa_Kupca"),
    @NamedQuery(name = "Kupac.findByBroj_Telefona", query = "SELECT k FROM Kupac k WHERE k.broj_Telefona = :broj_Telefona")})
public class Kupac implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Kupac")
    private Integer id_Kupac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "adresa_Kupca")
    private String adresa_Kupca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "broj_Telefona")
    private String broj_Telefona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kupac_id_Kupac")
    private Collection<Porudzbina> porudzbinaCollection;

    public Kupac() {
    }

    public Kupac(Integer id_Kupac) {
        this.id_Kupac = id_Kupac;
    }

    public Kupac(Integer id_Kupac, String adresa_Kupca, String broj_Telefona) {
        this.id_Kupac = id_Kupac;
        this.adresa_Kupca = adresa_Kupca;
        this.broj_Telefona = broj_Telefona;
    }

    public Integer getId_Kupac() {
        return id_Kupac;
    }

    public void setId_Kupac(Integer id_Kupac) {
        this.id_Kupac = id_Kupac;
    }

    public String getAdresa_Kupca() {
        return adresa_Kupca;
    }

    public void setAdresa_Kupca(String adresa_Kupca) {
        this.adresa_Kupca = adresa_Kupca;
    }

    public String getBroj_Telefona() {
        return broj_Telefona;
    }

    public void setBroj_Telefona(String broj_Telefona) {
        this.broj_Telefona = broj_Telefona;
    }

    @XmlTransient
    public Collection<Porudzbina> getPorudzbinaCollection() {
        return porudzbinaCollection;
    }

    public void setPorudzbinaCollection(Collection<Porudzbina> porudzbinaCollection) {
        this.porudzbinaCollection = porudzbinaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Kupac != null ? id_Kupac.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kupac)) {
            return false;
        }
        Kupac other = (Kupac) object;
        if ((this.id_Kupac == null && other.id_Kupac != null) || (this.id_Kupac != null && !this.id_Kupac.equals(other.id_Kupac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Kupac[ id_Kupac=" + id_Kupac + " ]";
    }
    
}
