/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ASUS
 */
@Embeddable
public class NaruceniProizvodPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Porudzbina_id_Porudzbina")
    private int porudzbinaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Proizvod_id_Proizvod")
    private int proizvodId;

    public NaruceniProizvodPK() {
    }

    public NaruceniProizvodPK(int porudzbina_id_Porudzbina, int proizvod_id_Proizvod) {
        this.porudzbinaId = porudzbina_id_Porudzbina;
        this.proizvodId = proizvod_id_Proizvod;
    }

    public int getPorudzbinaidPorudzbina() {
        return porudzbinaId;
    }

    public void setPorudzbinaidPorudzbina(int porudzbina_id_Porudzbina) {
        this.porudzbinaId = porudzbina_id_Porudzbina;
    }

    public int getProizvodidProizvod() {
        return proizvodId;
    }

    public void setProizvodidProizvod(int proizvod_id_Proizvod) {
        this.proizvodId = proizvod_id_Proizvod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) porudzbinaId;
        hash += (int) proizvodId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NaruceniProizvodPK)) {
            return false;
        }
        NaruceniProizvodPK other = (NaruceniProizvodPK) object;
        if (this.porudzbinaId != other.porudzbinaId) {
            return false;
        }
        if (this.proizvodId != other.proizvodId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.NaruceniProizvodPK[ porudzbina_id_Porudzbina=" + porudzbinaId + ", proizvod_id_Proizvod=" + proizvodId + " ]";
    }
    
}
