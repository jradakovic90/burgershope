/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "proizvod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proizvod.findAll", query = "SELECT p FROM Proizvod p"),
    @NamedQuery(name = "Proizvod.findById_Proizvod", query = "SELECT p FROM Proizvod p WHERE p.id_Proizvod = :id_Proizvod"),
    @NamedQuery(name = "Proizvod.findByIme_Proizvoda", query = "SELECT p FROM Proizvod p WHERE p.ime_Proizvoda = :ime_Proizvoda"),
    @NamedQuery(name = "Proizvod.findByCena", query = "SELECT p FROM Proizvod p WHERE p.cena = :cena"),
    @NamedQuery(name = "Proizvod.findByOpis_Sastojaka", query = "SELECT p FROM Proizvod p WHERE p.opis_Sastojaka = :opis_Sastojaka"),
    @NamedQuery(name = "Proizvod.findByAzuriranje", query = "SELECT p FROM Proizvod p WHERE p.azuriranje = :azuriranje")})
public class Proizvod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Proizvod")
    private Integer id_Proizvod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "ime_Proizvoda")
    private String ime_Proizvoda;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "cena")
    private BigDecimal cena;
    @Size(max = 255)
    @Column(name = "opis_Sastojaka")
    private String opis_Sastojaka;
    @Basic(optional = false)
    @NotNull
    @Column(name = "azuriranje")
    @Temporal(TemporalType.TIMESTAMP)
    private Date azuriranje;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proizvod")
    private Collection<NaruceniProizvod> naruceni_ProizvodCollection;
    @JoinColumn(name = "Kategorija_id_INT", referencedColumnName = "id_Kategorija")
    @ManyToOne(optional = false)
    private Kategorija kategorija_id_INT;

    public Proizvod() {
    }

    public Proizvod(Integer id_Proizvod) {
        this.id_Proizvod = id_Proizvod;
    }

    public Proizvod(Integer id_Proizvod, String ime_Proizvoda, BigDecimal cena, Date azuriranje) {
        this.id_Proizvod = id_Proizvod;
        this.ime_Proizvoda = ime_Proizvoda;
        this.cena = cena;
        this.azuriranje = azuriranje;
    }

    public Integer getId_Proizvod() {
        return id_Proizvod;
    }

    public void setId_Proizvod(Integer id_Proizvod) {
        this.id_Proizvod = id_Proizvod;
    }

    public String getIme_Proizvoda() {
        return ime_Proizvoda;
    }

    public void setIme_Proizvoda(String ime_Proizvoda) {
        this.ime_Proizvoda = ime_Proizvoda;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public String getOpis_Sastojaka() {
        return opis_Sastojaka;
    }

    public void setOpis_Sastojaka(String opis_Sastojaka) {
        this.opis_Sastojaka = opis_Sastojaka;
    }

    public Date getAzuriranje() {
        return azuriranje;
    }

    public void setAzuriranje(Date azuriranje) {
        this.azuriranje = azuriranje;
    }

    @XmlTransient
    public Collection<NaruceniProizvod> getNaruceniProizvodCollection() {
        return naruceni_ProizvodCollection;
    }

    public void setNaruceniProizvodCollection(Collection<NaruceniProizvod> naruceni_ProizvodCollection) {
        this.naruceni_ProizvodCollection = naruceni_ProizvodCollection;
    }

    public Kategorija getKategorijaidINT() {
        return kategorija_id_INT;
    }

    public void setKategorijaidINT(Kategorija kategorija_id_INT) {
        this.kategorija_id_INT = kategorija_id_INT;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Proizvod != null ? id_Proizvod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proizvod)) {
            return false;
        }
        Proizvod other = (Proizvod) object;
        if ((this.id_Proizvod == null && other.id_Proizvod != null) || (this.id_Proizvod != null && !this.id_Proizvod.equals(other.id_Proizvod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Proizvod[ id_Proizvod=" + id_Proizvod + " ]";
    }
    
}
