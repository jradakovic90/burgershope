/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JRadakovic
 */
@Entity
@Table(name = "kategorija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kategorija.findAll", query = "SELECT k FROM Kategorija k"),
    @NamedQuery(name = "Kategorija.findById_Kategorija", query = "SELECT k FROM Kategorija k WHERE k.id_Kategorija = :id_Kategorija"),
    @NamedQuery(name = "Kategorija.findByNaziv_Kategorije", query = "SELECT k FROM Kategorija k WHERE k.naziv_Kategorije = :naziv_Kategorije")})
public class Kategorija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Kategorija")
    private Integer id_Kategorija;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "naziv_Kategorije")
    private String naziv_Kategorije;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kategorija_id_INT")
    private Collection<Proizvod> proizvodCollection;

    public Kategorija() {
    }

    public Kategorija(Integer id_Kategorija) {
        this.id_Kategorija = id_Kategorija;
    }

    public Kategorija(Integer id_Kategorija, String naziv_Kategorije) {
        this.id_Kategorija = id_Kategorija;
        this.naziv_Kategorije = naziv_Kategorije;
    }

    public Integer getId_Kategorija() {
        return id_Kategorija;
    }

    public void setId_Kategorija(Integer id_Kategorija) {
        this.id_Kategorija = id_Kategorija;
    }

    public String getNaziv_Kategorije() {
        return naziv_Kategorije;
    }

    public void setNaziv_Kategorije(String naziv_Kategorije) {
        this.naziv_Kategorije = naziv_Kategorije;
    }

    @XmlTransient
    public Collection<Proizvod> getProizvodCollection() {
        return proizvodCollection;
    }

    public void setProizvodCollection(Collection<Proizvod> proizvodCollection) {
        this.proizvodCollection = proizvodCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Kategorija != null ? id_Kategorija.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kategorija)) {
            return false;
        }
        Kategorija other = (Kategorija) object;
        if ((this.id_Kategorija == null && other.id_Kategorija != null) || (this.id_Kategorija != null && !this.id_Kategorija.equals(other.id_Kategorija))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Kategorija[ id_Kategorija=" + id_Kategorija + " ]";
    }
    
}
