/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "naruceni_proizvod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NaruceniProizvod.findAll", query = "SELECT n FROM NaruceniProizvod n"),
    @NamedQuery(name = "NaruceniProizvod.findByPorudzbinaId", query = "SELECT n FROM NaruceniProizvod n WHERE n.naruceniProizvodPK.porudzbinaId = :porudzbinaId"),
    @NamedQuery(name = "NaruceniProizvod.findByProizvodId", query = "SELECT n FROM NaruceniProizvod n WHERE n.naruceniProizvodPK.proizvodId = :proizvodId"),
    @NamedQuery(name = "NaruceniProizvod.findByKolicina", query = "SELECT n FROM NaruceniProizvod n WHERE n.kolicina = :kolicina")})
public class NaruceniProizvod implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NaruceniProizvodPK naruceniProizvodPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Kolicina")
    private short kolicina;
    @JoinColumn(name = "Porudzbina_id_Porudzbina", referencedColumnName = "id_Porudzbina", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Porudzbina porudzbina;
    @JoinColumn(name = "Proizvod_id_Proizvod", referencedColumnName = "id_Proizvod", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proizvod proizvod;

    public NaruceniProizvod() {
    }

    public NaruceniProizvod(NaruceniProizvodPK naruceniProizvodPK) {
        this.naruceniProizvodPK = naruceniProizvodPK;
    }

    public NaruceniProizvod(NaruceniProizvodPK naruceniProizvodPK, short kolicina) {
        this.naruceniProizvodPK = naruceniProizvodPK;
        this.kolicina = kolicina;
    }

    public NaruceniProizvod(int porudzbinaId, int proizvodId) {
        this.naruceniProizvodPK = new NaruceniProizvodPK(porudzbinaId, proizvodId);
    }

    public NaruceniProizvodPK getNaruceniProizvodPK() {
        return naruceniProizvodPK;
    }

    public void setNaruceniProizvodPK(NaruceniProizvodPK naruceniProizvodPK) {
        this.naruceniProizvodPK = naruceniProizvodPK;
    }

    public short getKolicina() {
        return kolicina;
    }

    public void setKolicina(short kolicina) {
        this.kolicina = kolicina;
    }

    public Porudzbina getPorudzbina() {
        return porudzbina;
    }

    public void setPorudzbina(Porudzbina porudzbina) {
        this.porudzbina = porudzbina;
    }

    public Proizvod getProizvod() {
        return proizvod;
    }

    public void setProizvod(Proizvod proizvod) {
        this.proizvod = proizvod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (naruceniProizvodPK != null ? naruceniProizvodPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NaruceniProizvod)) {
            return false;
        }
        NaruceniProizvod other = (NaruceniProizvod) object;
        if ((this.naruceniProizvodPK == null && other.naruceniProizvodPK != null) || (this.naruceniProizvodPK != null && !this.naruceniProizvodPK.equals(other.naruceniProizvodPK))) {
            return false;
        }
        return true;
    }

 /*   @Override
    public String toString() {
      return "entity.Naruceni_Proizvod[ naruceniProizvodPK=" + naruceniProizvodPK + " ]";

    }
   */ 
}
