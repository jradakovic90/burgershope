/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "porudzbina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Porudzbina.findAll", query = "SELECT p FROM Porudzbina p"),
    @NamedQuery(name = "Porudzbina.findById_Porudzbina", query = "SELECT p FROM Porudzbina p WHERE p.id_Porudzbina = :id_Porudzbina"),
    @NamedQuery(name = "Porudzbina.findByRacun_Porudzbine", query = "SELECT p FROM Porudzbina p WHERE p.racun_Porudzbine = :racun_Porudzbine"),
    @NamedQuery(name = "Porudzbina.findByVreme_kreiranja_Porudzbine", query = "SELECT p FROM Porudzbina p WHERE p.vreme_kreiranja_Porudzbine = :vreme_kreiranja_Porudzbine"),
    @NamedQuery(name = "Porudzbina.findByBroj_potvrde_Porudzbine", query = "SELECT p FROM Porudzbina p WHERE p.broj_potvrde_Porudzbine = :broj_potvrde_Porudzbine")})
public class Porudzbina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Porudzbina")
    private Integer id_Porudzbina;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "racun_Porudzbine")
    private BigDecimal racun_Porudzbine;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vreme_kreiranja_Porudzbine")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vreme_kreiranja_Porudzbine;
    @Basic(optional = false)
    @NotNull
    @Column(name = "broj_potvrde_Porudzbine")
    private int broj_potvrde_Porudzbine;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "porudzbina")
    private Collection<NaruceniProizvod> naruceni_ProizvodCollection;
    @JoinColumn(name = "Kupac_id_Kupac", referencedColumnName = "id_Kupac")
    @ManyToOne(optional = false)
    private Kupac kupac_id_Kupac;

    public Porudzbina() {
    }

    public Porudzbina(Integer id_Porudzbina) {
        this.id_Porudzbina = id_Porudzbina;
    }

    public Porudzbina(Integer id_Porudzbina, BigDecimal racun_Porudzbine, Date vreme_kreiranja_Porudzbine, int broj_potvrde_Porudzbine) {
        this.id_Porudzbina = id_Porudzbina;
        this.racun_Porudzbine = racun_Porudzbine;
        this.vreme_kreiranja_Porudzbine = vreme_kreiranja_Porudzbine;
        this.broj_potvrde_Porudzbine = broj_potvrde_Porudzbine;
    }

    public Integer getIdPorudzbina() {
        return id_Porudzbina;
    }

    public void setIdPorudzbina(Integer id_Porudzbina) {
        this.id_Porudzbina = id_Porudzbina;
    }

    public BigDecimal getRacunPorudzbine() {
        return racun_Porudzbine;
    }

    public void setRacunPorudzbine(BigDecimal racun_Porudzbine) {
        this.racun_Porudzbine = racun_Porudzbine;
    }

    public Date getVremekreiranjaPorudzbine() {
        return vreme_kreiranja_Porudzbine;
    }

    public void setVremekreiranjaPorudzbine(Date vreme_kreiranja_Porudzbine) {
        this.vreme_kreiranja_Porudzbine = vreme_kreiranja_Porudzbine;
    }

    public int getBrojpotvrdePorudzbine() {
        return broj_potvrde_Porudzbine;
    }

    public void setBrojpotvrdePorudzbine(int broj_potvrde_Porudzbine) {
        this.broj_potvrde_Porudzbine = broj_potvrde_Porudzbine;
    }

    @XmlTransient
    public Collection<NaruceniProizvod> getNaruceniProizvodCollection() {
        return naruceni_ProizvodCollection;
    }

    public void setNaruceniProizvodCollection(Collection<NaruceniProizvod> naruceni_ProizvodCollection) {
        this.naruceni_ProizvodCollection = naruceni_ProizvodCollection;
    }

    public Kupac getKupacidKupac() {
        return kupac_id_Kupac;
    }

    public void setKupacidKupac(Kupac kupac_id_Kupac) {
        this.kupac_id_Kupac = kupac_id_Kupac;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Porudzbina != null ? id_Porudzbina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Porudzbina)) {
            return false;
        }
        Porudzbina other = (Porudzbina) object;
        if ((this.id_Porudzbina == null && other.id_Porudzbina != null) || (this.id_Porudzbina != null && !this.id_Porudzbina.equals(other.id_Porudzbina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Porudzbina[ id_Porudzbina=" + id_Porudzbina + " ]";
    }
    
}
