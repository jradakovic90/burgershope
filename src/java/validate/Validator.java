/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validate;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ASUS
 */
public class Validator {
    
    // applies to quantity fields in cart page
    public boolean validateQuantity (String id_Proizvod, String kvalitet) {

        boolean errorFlag = false;

        if (!id_Proizvod.isEmpty() && !kvalitet.isEmpty()) {

            int i = -1;

            try {

                i = Integer.parseInt(kvalitet);
            } catch (NumberFormatException nfe) {

                System.out.println("User did not enter a number in the quantity field");
            }

            if (i < 0 || i > 99) {

                errorFlag = true;
            }
        }

        return errorFlag;
    }


    // performs simple validation on checkout form
    public boolean validateForm(
                                String phone,
                                String address,
                                HttpServletRequest request) {

        boolean errorFlag = false;
        boolean phoneError;
        boolean addressError;
     

        
        
        if (phone == null
                || phone.equals("")
                || phone.length() < 9) {
            errorFlag = true;
            phoneError = true;
            request.setAttribute("phoneError", phoneError);
        }
        if (address == null
                || address.equals("")
                || address.length() > 45) {
            errorFlag = true;
            addressError = true;
            request.setAttribute("addressError", addressError);
        }
       
        return errorFlag;
    }

}
