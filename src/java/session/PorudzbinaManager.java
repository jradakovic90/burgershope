/*
 * Copyright (c) 2010, Oracle and/or its affiliates. All rights reserved.
 *
 * You may not modify, use, reproduce, or distribute this software
 * except in compliance with the terms of the license at:
 * http://developer.sun.com/berkeley_license.html
 */
package session;

import cart.*;
import entity.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jradakovic
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PorudzbinaManager {

    @PersistenceContext(unitName = "JelenasBurgersPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    @EJB
    private ProizvodFacade proizvodFacade;
    @EJB
    private NaruceniProizvodFacade naruceniProizvodFacade;
    @EJB
    private PorudzbinaFacade porudzbinaFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Porudzbina postaviPorudzbinu(String brojTelefona, String adresa, ShoppingCart korpa) {

        try {
            Kupac kupac = dodajKupca(brojTelefona, adresa);
            Porudzbina poruzdzbina = dodajPorudzbinu(kupac, korpa);
            dodajPoruceneStavke(poruzdzbina, korpa);
            return poruzdzbina;
        } catch (Exception e) {
            e.printStackTrace();
            context.setRollbackOnly();
            return null;
        }
    }

    private Kupac dodajKupca(String brojTelefona, String adresa) {

        Kupac kupac = new Kupac();
        kupac.setAdresa_Kupca(adresa);
        kupac.setBroj_Telefona(brojTelefona);
        Random randomIdKupac = new Random();
        int idKupac = randomIdKupac.nextInt(999999999);
        kupac.setId_Kupac(idKupac);
        em.persist(kupac);
        return kupac;
    }

    private Porudzbina dodajPorudzbinu(Kupac kupac, ShoppingCart korpa) {

        // set up customer order
        Porudzbina porudzbina = new Porudzbina();

        porudzbina.setKupacidKupac(kupac);
        porudzbina.setRacunPorudzbine(BigDecimal.valueOf(korpa.getTotal()));

        // create confirmation number
        Random random = new Random();
        int i = random.nextInt(999999999);
        porudzbina.setBrojpotvrdePorudzbine(i);
        Random randomIdPorudzbina = new Random();
        int idPorudzbina = randomIdPorudzbina.nextInt(999999999);
        porudzbina.setIdPorudzbina(idPorudzbina);
        porudzbina.setVremekreiranjaPorudzbine(new Date());
        em.persist(porudzbina);
        return porudzbina;
    }

    private void dodajPoruceneStavke(Porudzbina porudzbina, ShoppingCart korpa) {

        em.flush();

        List<ShoppingCartItem> items = korpa.getItems();

        // iterate through shopping cart and create OrderedProducts
        for (ShoppingCartItem scItem : items) {

            int productId = scItem.getProduct().getId_Proizvod();

            // set up primary key object
            NaruceniProizvodPK naruceniProizvodPK = new NaruceniProizvodPK();
            naruceniProizvodPK.setPorudzbinaidPorudzbina(porudzbina.getIdPorudzbina());
            naruceniProizvodPK.setProizvodidProizvod(productId);

            // create ordered item using PK object
            NaruceniProizvod narucenaStavka = new NaruceniProizvod(naruceniProizvodPK);

            // set quantity
            narucenaStavka.setKolicina(scItem.getKolicina());

            em.persist(narucenaStavka);
        }
    }

    public Map getDetaljiPorudzbine(Porudzbina porudzbina) {

        Map orderMap = new HashMap();

        // get customer
        Kupac kupac = porudzbina.getKupacidKupac();

        // get all ordered products
        List<NaruceniProizvod> naruceniProizvodi = naruceniProizvodFacade.findByOrderId(porudzbina.getIdPorudzbina());

        // get product details for ordered items
        List<Proizvod> proizvodi = new ArrayList<Proizvod>();

        for (NaruceniProizvod op : naruceniProizvodi) {

            Proizvod p = (Proizvod) proizvodFacade.find(op.getNaruceniProizvodPK().getProizvodidProizvod());
            proizvodi.add(p);
        }

        // add each item to orderMap
        orderMap.put("porudzbina", porudzbina);
        orderMap.put("kupac", kupac);
        orderMap.put("naruceniProizvodi", naruceniProizvodi);
        orderMap.put("proizvodi", proizvodi);

        return orderMap;
    }

}
