/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entity.Proizvod;

/**
 *
 * @author ASUS
 */
public class ShoppingCartItem {
    
 Proizvod proizvod;
    short kolicina;

    public ShoppingCartItem(Proizvod proizvod) {
        this.proizvod = proizvod;
        kolicina = 1;
    }

    public Proizvod getProduct() {
        return proizvod;
    }

    public short getKolicina() {
        return kolicina;
    }

    public void setKolicina(short kolicina) {
        this.kolicina = kolicina;
    }

    public void incrementQuantity() {
       kolicina++;
    }

    public void decrementQuantity() {
        kolicina--;
    }

    public double getTotal() {
        double amount = 0;
        amount = (this.getKolicina() * proizvod.getCena().doubleValue());
        return amount;
    }

}
